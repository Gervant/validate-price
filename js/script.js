//Обработчик событий это функционал позволяющий связывать объекты DOM с действияи пользователя и таким образом
//добавлять, удалять и изменять содержимое дерева.
const warnSpan = document.createElement('span');
const inputValue = document.createElement('input');


inputValue.type = 'number';
document.body.append(inputValue);
inputValue.addEventListener('focus', event => {
    inputValue.style.outline = 'none';
    inputValue.style.border ='2px solid green';
    inputValue.placeholder = 'Price';
    inputValue.value = ''
    warnSpan.remove()
})
inputValue.addEventListener('blur', event => {
    inputValue.style.border = '2px solid grey';
    if (inputValue.value){

        if (inputValue.value < 0 ){

            warnSpan.innerText = 'Please enter correct price';
            warnSpan.style.display = 'block';
            document.body.append(warnSpan);
            inputValue.style.border = '2px solid red';

        } else {
            const priceSpan = document.createElement('span');
            priceSpan.style.display = 'block';
            priceSpan.innerText = `Текущая цена: ${inputValue.value}`;
            const button = document.createElement('button');
            button.innerText = 'X';

            document.body.prepend(priceSpan);
            priceSpan.append(button);


            button.addEventListener('click', event => {
                priceSpan.remove();
                inputValue.value = '';
            })
        }
    }
})
